<?php

namespace Tests;

use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    /**
     * This method is called before each test.
     */
    protected function setUp() : void
    {
        parent::setUp();
    }

    /**
     * Signs in a user
     * if no user is passed, a new one is created from factory
     * @param type|null $user
     * @return type
     */
    public function signIn($user = null)
    {
        $user = $user ?: create('App\Models\User');

        $this->actingAs($user);

        return $this;
    }
}
