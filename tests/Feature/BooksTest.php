<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class BooksTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Runs at the begining of each test
     * @return type
     */
    public function setUp() : void
    {
        parent::setup();
        $this->user = create('App\Models\User');
        $this->tag = create('App\Models\Tag');
        $this->nd_tag = create('App\Models\Tag');
    }


    /** @test */
    public function a_user_can_add_a_book()
    {
        $this->withoutExceptionHandling();

        $book = make('App\Models\Book');
        $data = $book->toArray();

        $data = array_merge($data, [
            'tags' => [
                0 => $this->tag->id,
                1 => $this->nd_tag->id
            ],
            'cover_image' => UploadedFile::fake()->image('cover.jpg')
        ]);

        $this->signIn($this->user)
            ->post(
                '/books/create',
                $data
            )
            ->assertStatus(200);

        $this->assertDatabasehas('books', [
            'name' => $book->name,
            'description' => $book->description,
            'author_id' => $book->author_id,
            'creator_id' => $this->user->id,
            'cover_image_url' => 'storage/book_covers/1/cover.jpg'
        ]);

        Storage::disk('local')->assertExists('public/book_covers/1/cover.jpg');
    }

    /** @test */
    public function when_adding_a_book_tags_get_saved()
    {
        $this->withoutExceptionHandling();

        $book = make('App\Models\Book');
        $data = $book->toArray();

        $data = array_merge($data, [
            'tags' => [
                0 => $this->tag->id,
                1 => $this->nd_tag->id
            ],
            'cover_image' => UploadedFile::fake()->image('cover.jpg')
        ]);

        $response = $this->signIn($this->user)
            ->post(
                '/books/create',
                $data
            )
            ->assertStatus(200);

        $book = json_decode($response->content())->book;

        $this->assertDatabasehas('book_tag', [
            'book_id' => $book->id,
            'tag_id' => $this->tag->id
        ]);

        $this->assertDatabasehas('book_tag', [
            'book_id' => $book->id,
            'tag_id' => $this->nd_tag->id
        ]);
    }

    /** @test */
    public function a_user_can_edit_a_book()
    {
        $this->withoutExceptionHandling();

        $book = create('App\Models\Book', ['creator_id' => $this->user->id]);
        $edited = make('App\Models\Book');

        $data = $edited->toArray();

        $this->signIn($this->user)
            ->patch(
                '/books/edit/' . $book->id,
                $data
            )
            ->assertStatus(200);

        $this->assertDatabasehas('books', [
            'name' => $edited->name,
            'description' => $edited->description,
            'author_id' => $edited->author_id,
            'creator_id' => $this->user->id,
        ]);
    }

    /** @test */
    public function when_editing_a_book_tags_can_be_updated()
    {
        $this->withoutExceptionHandling();

        $book = create('App\Models\Book', ['creator_id' => $this->user->id]);
        DB::table('book_tag')->insert([
            'book_id' => $book->id, 'tag_id' => $this->tag->id
        ]);

        DB::table('book_tag')->insert([
            'book_id' => $book->id, 'tag_id' => $this->nd_tag->id
        ]);


        $edited = make('App\Models\Book');
        $new_tag = create('App\Models\Tag');
        $nd_new_tag = create('App\Models\Tag');


        $data = $edited->toArray();

        $data = array_merge($data, [
            'tags' => [
                0 => $new_tag->id,
                1 => $nd_new_tag->id
            ],
            'cover_image' => UploadedFile::fake()->image('new_cover.jpg')
        ]);

        $this->signIn($this->user)
            ->patch(
                '/books/edit/' . $book->id,
                $data
            )
            ->assertStatus(200);

        $this->assertDatabasehas('book_tag', [
            'book_id' => $book->id,
            'tag_id' => $new_tag->id
        ]);

        $this->assertDatabasehas('book_tag', [
            'book_id' => $book->id,
            'tag_id' => $nd_new_tag->id
        ]);

        $this->assertDatabaseMissing('book_tag', [
            'book_id' => $book->id,
            'tag_id' => $this->tag->id
        ]);

        $this->assertDatabaseMissing('book_tag', [
            'book_id' => $book->id,
            'tag_id' => $this->nd_tag->id
        ]);
    }

    /** @test */
    public function when_editing_a_book_the_cover_image_can_be_updated()
    {
        $this->withoutExceptionHandling();

        $book = create('App\Models\Book', ['creator_id' => $this->user->id]);
        $edited = make('App\Models\Book');

        $data = $edited->toArray();
        $data = array_merge($data, [
            'cover_image' => UploadedFile::fake()->image('new_cover.jpg')
        ]);

        $this->signIn($this->user)
            ->patch(
                '/books/edit/' . $book->id,
                $data
            )
            ->assertStatus(200);

        $this->assertDatabasehas('books', [
            'cover_image_url' => 'storage/book_covers/1/new_cover.jpg'
        ]);

        Storage::disk('local')->assertExists('public/book_covers/1/new_cover.jpg');
    }

    /** @test */
    public function a_user_can_delete_a_book()
    {
        $this->withoutExceptionHandling();

        $book = create('App\Models\Book', ['creator_id' => $this->user->id]);

        $this->signIn($this->user)
            ->delete(
                '/books/delete/' . $book->id
            )
            ->assertStatus(200);

        $this->assertDatabaseMissing('books', [
            'name' => $book->name,
            'description' => $book->description,
            'author_id' => $book->author_id,
            'creator_id' => $this->user->id,
        ]);
    }

    /** @test */
    public function when_deleting_a_book_the_tags_get_unattached()
    {
        $this->withoutExceptionHandling();

        $book = create('App\Models\Book', ['creator_id' => $this->user->id]);
        DB::table('book_tag')->insert([
            'book_id' => $book->id, 'tag_id' => $this->tag->id
        ]);

        DB::table('book_tag')->insert([
            'book_id' => $book->id, 'tag_id' => $this->nd_tag->id
        ]);


        $this->signIn($this->user)
            ->delete(
                '/books/delete/' . $book->id
            )
            ->assertStatus(200);

        $this->assertDatabaseMissing('book_tag', [
            'book_id' => $book->id,
            'tag_id' => $this->tag->id
        ]);

        $this->assertDatabaseMissing('book_tag', [
            'book_id' => $book->id,
            'tag_id' => $this->nd_tag->id
        ]);

        // Storage::disk('local')->assertExists('photo1.jpg');
    }

    /** @test */
    public function when_deleting_a_book_the_cover_image_gets_removed()
    {
        $this->withoutExceptionHandling();

        $this->withoutExceptionHandling();

        $book = make('App\Models\Book');
        $data = $book->toArray();

        $data = array_merge($data, [
            'tags' => [
                0 => $this->tag->id,
                1 => $this->nd_tag->id
            ],
            'cover_image' => UploadedFile::fake()->image('cover.jpg')
        ]);

        $response = $this->signIn($this->user)
            ->post(
                '/books/create',
                $data
            )
            ->assertStatus(200);
        Storage::disk('local')->assertExists('public/book_covers/1/cover.jpg');
        $book = json_decode($response->content())->book;

        $this->signIn($this->user)
            ->delete(
                '/books/delete/' . $book->id
            )
            ->assertStatus(200);

        Storage::disk('local')->assertMissing('public/book_covers/'. $book->id .'/cover.jpg');
    }

    /** @test */
    public function a_user_can_get_a_list_of_books()
    {
        $this->withoutExceptionHandling();
        $book = create('App\Models\Book');
        $nd_book = create('App\Models\Book');

        $response = $this->signIn($this->user)
            ->get(
                '/books'
            )
            ->assertStatus(200);
        $books = json_decode($response->content())->books;

        $this->assertEquals(2, sizeof($books));
    }

    /** @test */
    public function when_getting_books_tas_are_returned_as_well()
    {
        $this->withoutExceptionHandling();
        $book = create('App\Models\Book');
        $nd_book = create('App\Models\Book');
        DB::table('book_tag')->insert([
            'book_id' => $book->id, 'tag_id' => $this->tag->id
        ]);
        DB::table('book_tag')->insert([
            'book_id' => $book->id, 'tag_id' => $this->nd_tag->id
        ]);
        DB::table('book_tag')->insert([
            'book_id' => $nd_book->id, 'tag_id' => $this->tag->id
        ]);

        DB::table('book_tag')->insert([
            'book_id' => $nd_book->id, 'tag_id' => $this->nd_tag->id
        ]);

        $response = $this->signIn($this->user)
            ->get(
                '/books'
            )
            ->assertStatus(200);
        $books = json_decode($response->content())->books;

        foreach ($books as $book) {
            $this->assertEquals(2, sizeof($book->tags));
        }
    }
}
