<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class BooksTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Runs at the begining of each test
     * @return type
     */
    public function setUp() : void
    {
        parent::setup();
        $this->author = create('App\Models\Author');
        $this->book = create('App\Models\Book', ['author_id' => $this->author->id]);
    }

    /** @test */
    public function a_book_belongs_to_an_author()
    {
        $this->assertInstanceOf('App\Models\Author', $this->book->author);
    }

    /** @test */
    public function a_book_has_many_tags()
    {
        $tag = create('App\Models\Tag');
        $nd_tag = create('App\Models\Tag');

        $this->book->tags()->attach($tag->id);
        $this->book->tags()->attach($nd_tag->id);

        foreach ($this->book->tags as $tag) {
            $this->assertInstanceOf('App\Models\Tag', $tag);
        }

        $this->assertEquals(2, $this->book->tags->count());
    }
}
