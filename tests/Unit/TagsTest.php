<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class TagsTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Runs at the begining of each test
     * @return type
     */
    public function setUp() : void
    {
        parent::setup();
        $this->tag =  create('App\Models\Tag');
    }

    /** @test */
    public function a_tag_belongs_to_many_books()
    {
        $book = create('App\Models\Book');
        $nd_book = create('App\Models\Book');

        $this->tag->books()->attach($book->id);
        $this->tag->books()->attach($nd_book->id);

        foreach ($this->tag->books as $book) {
            $this->assertInstanceOf('App\Models\Book', $book);
        }

        $this->assertEquals(2, $this->tag->books->count());
    }
}
