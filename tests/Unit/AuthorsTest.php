<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AuthorsTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Runs at the begining of each test
     * @return type
     */
    public function setUp() : void
    {
        parent::setup();
        $this->author = create('App\Models\Author');
        $this->book = create('App\Models\Book', ['author_id' => $this->author->id]);
    }

    /** @test */
    public function an_author_has_many_books()
    {
        foreach ($this->author->books as $book) {
            $this->assertInstanceOf('App\Models\Book', $book);
        }
    }
}
