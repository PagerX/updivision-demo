<?php

namespace App\Http\Controllers;

use App\Models\Tag;
use App\Models\Book;
use App\Models\Author;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class BookController extends Controller
{
    /**
     * Validates a create book request
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function bookvalidator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string',
            'description' => 'required|string',
            'cover_image' => 'required|image',
            'author_id' => 'required|integer',
            'tags' => 'required|array'
        ]);
    }

    /**
     * Validates a create book request
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function bookEditValidator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string',
            'description' => 'required|string',
            'author_id' => 'required|integer',
        ]);
    }


    /**
     * Return a list of books
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $books = Book::all()->load('author', 'tags');

        return $this->respond($books, 'books');
    }

    /**
     * Store a new book
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = Auth::user();
        $request['tags'] = $this->arrayOrJson($request['tags']);
        $this->bookvalidator($request->all())->validate();

        $book = $user->books()->create([
            'name' => $request['name'],
            'description' => $request['description'],
            'author_id' => $request['author_id'],
        ]);

        //we set the file name based on client original name
        $file_name = $request->file('cover_image')->getClientOriginalName();
        //we store the file
        $request->file('cover_image')
            ->storeAs('public/book_covers/'.$book->id.'/', $file_name);
        $path = 'storage/book_covers/' . $book->id . '/' . $file_name;
        $real_path = 'public/book_covers/' . $book->id . '/' . $file_name;
        //we save the url to our book
        $book->cover_image_url = $path;
        $book->real_image_path = $real_path;
        $book->save();

        $book->tags()->attach($request['tags']);

        return $this->respond($book, 'book');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function edit(Book $book, Request $request)
    {
        $request['tags'] = $this->arrayOrJson($request['tags']);
        $this->bookEditValidator($request->all())->validate();

        if(!is_null($request->file('cover_image'))) {
            Storage::delete($book->real_image_path);
            //we set the file name based on client original name
            $file_name = $request->file('cover_image')->getClientOriginalName();
            //we store the file
            $request->file('cover_image')
                ->storeAs('public/book_covers/'.$book->id.'/', $file_name);
            $path = 'storage/book_covers/' . $book->id . '/' . $file_name;
            $real_path = 'public/book_covers/' . $book->id . '/' . $file_name;
            //we save the url to our book
            $book->cover_image_url = $path;
            $book->real_image_path = $real_path;
            $book->save();
        }

        $book->update($request->all());

        if(array_key_exists('tags', $request->all()) && sizeof($request['tags']) > 0) {
            $book->tags()->sync($request['tags']);
        }

        return $this->respond($book, 'book');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function destroy(Book $book)
    {
        $book->delete();
        Storage::delete($book->real_image_path);

        return $this->respondWithDone(200, 'Success');
    }

    /**
     * Gets all authors and tags
     * @param Request $request
     * @return JSOn response
     */
    public function getAuthorsAndTags(Request $request)
    {
        $authors = Author::all();
        $tags = Tag::all();

        return $this->respond(['authors' => $authors, 'tags' => $tags], 'data', true);
    }
}
