<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    protected $guarded = ['creator_id'];
    protected $fillable = ['name', 'description', 'author_id'];

    /**
     * The author this book belongs to
     */
    public function author()
    {
        return $this->belongsTo(Author::class);
    }

    /**
     * The tags that belong to the book.
     */
    public function tags()
    {
        return $this->belongsToMany(Tag::class, 'book_tag', 'book_id', 'tag_id');
    }
}
