<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    protected $guarded = [];

    /**
     * The books that belong to the tag.
     */
    public function books()
    {
        return $this->belongsToMany(Book::class, 'book_tag', 'tag_id', 'book_id');
    }
}
