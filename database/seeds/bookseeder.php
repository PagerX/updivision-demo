<?php

use Illuminate\Database\Seeder;

class bookseeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\Book::class, 12)->create()->each(function ($book) {
            $book->tags()->attach(factory(App\Models\Tag::class)->create());
        });
    }
}
