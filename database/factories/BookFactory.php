<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Models\Book;
use App\Model;
use Faker\Generator as Faker;

$factory->define(Book::class, function (Faker $faker) {
    return [
        'name' => $faker->name(),
        'description' => $faker->sentence(10),
        'cover_image_url' => $faker->imageUrl(),
        'author_id' => factory('App\Models\Author'),
        'creator_id' => factory('App\Models\User')
    ];
});
