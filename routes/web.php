<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');

Route::prefix('books')->group( function () {
    Route::get('/', 'BookController@index');
    Route::post('/create', 'BookController@store');
    Route::patch('/edit/{book}', 'BookController@edit');
    Route::delete('/delete/{book}', 'BookController@destroy');
});

Route::get('/tags-and-authors', 'BookController@getAuthorsAndTags');
