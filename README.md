# updivision-demo

To setup the project please follow the next steps:

1)Open up a terminal in the project folder
2) Run composer install
3) Run php artisan storage:link
4) Run php artisan dump-autoload
5) Run npm install
6) Setup .env following the provided .env.example file*, I've used sqlite as a DB_CONNECTION, but feel free to use a local MYSQL db
7) Run php artisan migrate --seed
8) Run npm run dev
9) Run php artisan serve
10) The app can now be accessed on the returned port
*
*
*
Tests can be run using "vendor\bin\phpunit" in the terminal (if using a mac, should be vendor/bin/phpunit I believe)
*
*
